package com.javagda25.securitytemplate.repository;

import com.javagda25.securitytemplate.model.UserPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPhotoRepository extends JpaRepository<UserPhoto, Long> {
}
